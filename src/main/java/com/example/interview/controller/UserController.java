package com.example.interview.controller;

import com.example.interview.model.Category;
import com.example.interview.model.User;
import com.example.interview.service.UserService;
import net.bytebuddy.implementation.bytecode.Throw;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class UserController {
    @Autowired
    UserService userService;

    @RequestMapping(value="user", method= RequestMethod.POST)
    public ResponseEntity createUser(@RequestBody User user) {
        List<User> users = userService.getUsers();

        //Unique username
        for (User u : users) {
            if (u.getUsername().equalsIgnoreCase(user.getUsername())) {
                return ResponseEntity.status(HttpStatus.ALREADY_REPORTED).body("Username already exists!");
            }
        }

        //Validate user category
        if((user.getCategory().toLowerCase()).equals(Category.CANDIDATE.label)){
            user.setCategory(Category.CANDIDATE.label);
        }
        else if((user.getCategory().toLowerCase()).equals(Category.INTERVIEWER.label)) {
            user.setCategory(Category.INTERVIEWER.label);
        }else {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(
                    "User category invalid. Use: "  + Category.CANDIDATE.label + " or " + Category.INTERVIEWER.label
            );
        }
        user.setUsername(user.getUsername().toLowerCase());
        User newUser = userService.createUser(user);
        return ResponseEntity.ok(newUser.getCategory() +  " user created [id: " + newUser.getId() + "]");
    }

    @RequestMapping(value="/users", method=RequestMethod.GET)
    public List<User> getUsers() {
        return userService.getUsers();
    }

    @RequestMapping(value="/user/{id}", method=RequestMethod.GET)
    public Optional<User> getUserById(@PathVariable(value = "id") Long id) {
        return userService.getUserById(id);
    }

    @RequestMapping(value="/user/{id}", method=RequestMethod.DELETE)
    public ResponseEntity deleteUser(@PathVariable(value = "id") Long id) {
       try{
           userService.deleteUser(id);
       }catch (Exception e){
           return ResponseEntity.notFound().build();
       }
        return ResponseEntity.ok("User deleted");
    }

}

