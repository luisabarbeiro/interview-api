package com.example.interview.controller;

import com.example.interview.model.Slot;
import com.example.interview.model.User;
import com.example.interview.model.Weekday;
import com.example.interview.repository.UserRepository;
import com.example.interview.service.SlotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping("/api")
public class SlotController {
    SlotService slotService;
    UserRepository userRepository;

    @Autowired
    public SlotController(SlotService slotService, UserRepository userRepository) {
        this.slotService = slotService;
        this.userRepository = userRepository;
    }

    @RequestMapping(value="slot", method= RequestMethod.POST)
    public ResponseEntity createUser(@RequestBody Slot slot) {

       //verify if userId is valid
        Optional<User> optionalUser = userRepository.findById(slot.getUser().getId());
        if(!optionalUser.isPresent()) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("Invalid user ID");
        }

        //verify if start is valid
        if(!validateTimeFormat(slot.getStartTime())){
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("Invalid start time format");
        }
        if(!validateTimeMinutes(slot.getStartTime())){
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("Invalid start time. Only full hours available [00:00]");
        }
        if(!validateTimeFormat(slot.getEndTime())){
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("Invalid start time format");
        }
        if(!validateTimeMinutes(slot.getEndTime())){
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("Invalid start time. Only full hours available [00:00]");
        }

        //Validate if slots exists
        int dayValue = getDayOrder(slot.getWeekday());
        slot.setDayOrder(dayValue);

        if(slotService.countSlotByUserAndFields(slot.getUser_id(),
                        slot.getStartTime(), slot.getEndTime(), slot.getDayOrder()) != 0){
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("Slot already exists");
        }

        //verify if weekday is valid and return weekday order
        if(dayValue == -1) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("Invalid day");
        }else {

            slot.setWeekday(slot.getWeekday().toUpperCase());

            slotService.createSlot(slot);
        }
        return ResponseEntity.ok("Slot created");
    }

    @RequestMapping(value="/available-slots", method=RequestMethod.GET)
    public List<Slot> getAvailableSlots(@RequestParam List<String> values) {
        List<Slot> availableSlots = new ArrayList<>();
        List<String> interviewers = values.subList(1, values.size());

        if(values.size() == 0){
            return null;
        }
        List<Slot> candidateSlots = slotService.getSlotsByUserId(Long.valueOf(values.get(0)).longValue());
        for(Slot s : candidateSlots){
            availableSlots.addAll(slotService.getSlotsByUsers(
                    interviewers, s.getStartTime(), s.getEndTime(), s.getDayOrder()));
            if(availableSlots.size() == interviewers.size()){
                break;
            }
            if(availableSlots.size() < interviewers.size()){
                availableSlots.clear();
            }

        }
        return availableSlots;
    }

    @RequestMapping(value="/slots", method=RequestMethod.GET)
    public List<Slot> getSlots() {
        return slotService.getAllSlots();
    }

    @RequestMapping(value="/slot/{id}", method=RequestMethod.DELETE)
    public ResponseEntity deleteSlot(@PathVariable(value = "id") Long id) {
        Boolean isError;
        try{
            slotService.deleteSlot(id);
        }catch (Exception e){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok("Slot deleted");
    }


    private int getDayOrder (String day) {
        for(Weekday w : Weekday.values()){
            if(w.toString().equals(day.toUpperCase())){
                return w.value;
            }
        }
        return -1;
    }

    private boolean validateTimeFormat (String time){
        DateFormat dft =  new SimpleDateFormat("HH:mm");
        dft.setLenient(false);
        try {
            dft.parse(time);
        } catch (ParseException e) {
            return false;
        }
        return true;
    }

    private Boolean validateTimeMinutes (String time){
       String [] parts = time.split(":");
       if(Integer.parseInt(parts[parts.length-1]) != 0){
           return false;
       }
        return true;
    }
}
