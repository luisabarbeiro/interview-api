package com.example.interview.repository;

import com.example.interview.model.Slot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.List;

public interface SlotRepository  extends JpaRepository<Slot, Long> {
    @Query(value = "SELECT * FROM slot u WHERE u.user_id = ?1 ORDER BY u.day_order, u.start_time",
            nativeQuery = true)
    List<Slot> findSlotsByCandidateId(Long userId);

    @Query(value = "SELECT * FROM slot WHERE " +
            "slot.user_id IN ?1 AND " +
            "slot.start_time = ?2 AND slot.end_time = ?3 AND slot.day_order = ?4"
            , nativeQuery = true)
    List<Slot> findSlotsByUsers(List<String> intIds, String start, String end, int dayOrder);

    @Query(value = "SELECT COUNT(*) FROM slot WHERE " +
            "slot.user_id = ?1 AND " +
            "slot.start_time = ?2 AND slot.end_time = ?3 AND slot.day_order = ?4"
            , nativeQuery = true)
    int countSlotsByUserAndFields(Long id, String start, String end, int dayOrder);

}
