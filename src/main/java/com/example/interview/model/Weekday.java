package com.example.interview.model;

public enum Weekday {
    MONDAY(1), TUESDAY(2), WEDNESDAY(3), THURSDAY(4), FRIDAY(5), ALL(0);

    public final int value;

    Weekday(int value) {
        this.value = value;
    }
}
