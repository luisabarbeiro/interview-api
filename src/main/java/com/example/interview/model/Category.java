package com.example.interview.model;

public enum Category {
    CANDIDATE ("candidate"),
    INTERVIEWER ("interviewer");

    public final String label;

    Category(String label) {
        this.label = label;
    }
}
