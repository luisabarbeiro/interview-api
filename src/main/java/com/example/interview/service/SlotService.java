package com.example.interview.service;

import com.example.interview.model.Slot;
import com.example.interview.repository.SlotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SlotService {
    @Autowired
    SlotRepository slotRepository;

    // CREATE
    public Slot createSlot(Slot slot) {
        return slotRepository.save(slot);
    }

    // READ
    public List<Slot> getAllSlots() {
        return slotRepository.findAll(Sort.by("dayOrder"));
    }

    public List<Slot> getSlotsByUserId(Long userId) {
        return slotRepository.findSlotsByCandidateId(userId);
    }

    public List<Slot> getSlotsByUsers(List<String> intId, String start, String end, int dayOrder) {
        return slotRepository.findSlotsByUsers(intId, start, end, dayOrder);
    }

    public int countSlotByUserAndFields(Long id, String start, String end, int dayOrder) {
        return slotRepository.countSlotsByUserAndFields(id, start, end, dayOrder);
    }

    // DELETE
    public void deleteSlot(Long id) {
        slotRepository.deleteById(id);
    }
}
