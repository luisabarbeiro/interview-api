-- Schema creation
CREATE DATABASE interview_schema;

CREATE TABLE interview_schema.users (
	id INT NOT NULL AUTO_INCREMENT,
	username VARCHAR(100) NOT NULL,
    first_name VARCHAR(45) NOT NULL,
    last_name VARCHAR(45) NOT NULL,
    category VARCHAR(15) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (category_id) REFERENCES user_category(id)
);
CREATE TABLE interview_schema.slot (
   id INT auto_increment NOT NULL,
   user_id INT NOT NULL,
   start_time TIME NOT NULL,
   end_time TIME NOT NULL,
   weekday VARCHAR(15) NOT NULL,
   day_order INT NOT NULL,
   PRIMARY KEY (id),
   FOREIGN KEY (user_id) REFERENCES User(id)
)